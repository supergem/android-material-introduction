package tn.supergem.materialinroductionlibrary;

import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;

import tn.supergem.androidmaterialintroduction.CustomAction;
import tn.supergem.androidmaterialintroduction.IntroductionActivity;
import tn.supergem.androidmaterialintroduction.IntroductionFragment;
import tn.supergem.androidmaterialintroduction.widgets.LinePageIndicatorEngine;
import tn.supergem.androidmaterialintroduction.widgets.PageIndicator;

public class MainActivity extends IntroductionActivity {

    private static final int[] BACKGROUND_COLORS = {
            Color.parseColor("#F44336"),
            Color.parseColor("#e9a83b"),
            Color.parseColor("#5b9899"),
            Color.parseColor("#265963"),
            Color.parseColor("#cbb6c5"),
            Color.parseColor("#4FC3F7"),
            Color.parseColor("#3F51B5"),
            Color.parseColor("#F44336")};

    @Override
    public String getIgnoreText() {
        return "SKIP";
    }

    @Override
    public Class getNextActivity() {
        return HomeActivity.class;
    }

    @Override
    public Bundle getNextActivityBundle() {
        return null;
    }

    /**
     * Requesting an IndicatorEngine to draw dots
     * Lib comes with 6 Engines built in:
     * DefaultPageIndicatorEngine (Material Design)
     * GoogleNowLauncherIndicatorEngine (Similar to the indicators in Google Now Launcher)
     * SimplePageIndicatorEngine (Simple dot color change on page change)
     * TyzenPageIndicatorEngine (Lines that rotate on page select)
     * RingPageIndicatorEngine (Rings that color up, two animations available)
     * LinePageIndicatorEngine (Line that stretches like progress or line can be constant size and move around)
     *
     * @return PageIndicatorEngine class which draws the indicator dots
     */
    @Override
    public PageIndicator.Engine getPageIndicatorEngine() {
        return new LinePageIndicatorEngine();
    }

    @Override
    public int getCount() {
        return BACKGROUND_COLORS.length;
    }

    @Override
    public int getBackgroundColor(int position) {
        return BACKGROUND_COLORS[position];
    }

    @Override
    public int getNavigationBarColor(int position) {
        return BACKGROUND_COLORS[position];
    }

    @Override
    public int getStatusBarColor(int position) {
        return BACKGROUND_COLORS[position];
    }

    @Override
    public Fragment getTutorialFragmentFor(int position) {
        switch (position) {
            case 3:
            case 4:
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("http://www.github.com/cadialex/MaterialTutorial"));
                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
                return new IntroductionFragment.Builder()
                        .setTitle("Title")
                        .setDescription("Desc")
                        .setImageResourceBackground(R.drawable.device)
                        .setImageResourceForeground(R.mipmap.ic_launcher)
                        .setCustomAction(
                                new CustomAction.Builder(pendingIntent)
                                        .setTitle("GitHu")
                                        .setIcon(R.drawable.ic_open_in_browser_white_24px)
                                        .build())
                        .build();
            default:
                return new IntroductionFragment.Builder()
                        .setTitle("Title")
                        .setDescription("Desc")
                        .setImageResourceBackground(R.drawable.device)
                        .setImageResourceForeground(R.mipmap.ic_launcher)
                        .build();
        }
    }

    @Override
    public boolean isNavigationBarColored() {
        return true;
    }

    @Override
    public boolean isStatusBarColored() {
        return true;
    }

    @Override
    public ViewPager.PageTransformer getPageTransformer() {
        return IntroductionFragment.getParallaxPageTransformer(1.25f);
    }
}
